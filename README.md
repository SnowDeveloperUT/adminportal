### Subscription Info Backend (SIB) - Admin back end ###

### Technologies used:
	* Back End:
	* Java 8,
	* Spring Boot,
	* Spring Data,
	* Spring Security,
	* Hibernate,
	* MySQL

### Features:
	* Responsive
	* Session management
	* Admin can view both user and music account table
	* Admin can disable a user (When user deactivates)
	
###  Set up steps:
	* If angular-cli is not there then it needs to be installed(npm install -g angular/cli)
	* Frontend needs to be running
	* Run from the folder content the command ng serve in Command Prompt